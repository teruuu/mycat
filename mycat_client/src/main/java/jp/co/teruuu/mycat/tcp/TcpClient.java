package jp.co.teruuu.mycat.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import jp.co.teruuu.mycat.config.AppProperties;


@Component
@ConfigurationProperties(prefix = "app")
public class TcpClient {
	@Autowired
	private AppProperties appProp;
	
	public void run() throws Exception{
		try(Socket socket = new Socket(appProp.getHost(), appProp.getPort())){
			clientSend(socket, appProp.getClientsend());
			clientReceive(socket);
		} catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void clientSend(Socket socket, String clientSendText) throws IOException{
		OutputStream output = socket.getOutputStream();
		for (char ch: clientSendText.toCharArray()) {
			output.write(ch);
		}
		output.write(0);
		System.out.println("client send:" + clientSendText);
	}

	public void clientReceive(Socket socket) throws IOException{
		int ch;
		String text = "";
		//クライアントから受け取った内容をserver_recv.txtに出力
		InputStream input = socket.getInputStream();
		// クライアントは、終了のマークとして0を送付してくる
		while((ch = input.read()) != 0){
			text += (char)ch;
		}
		System.out.println("client receive:" + text);
	}
}
